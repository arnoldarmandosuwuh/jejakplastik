import { StyleSheet, Text, View, TouchableOpacity, Image } from 'react-native'
import React from 'react'

const SurveyItem = ({ onPress, icon, title }) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.item} activeOpacity={0.7}>
      <Image source={{ uri: icon }} style={styles.imgItem} />
      <Text style={styles.desc}>{title}</Text>
    </TouchableOpacity>
  )
}

export default SurveyItem

const styles = StyleSheet.create({
  item: {
    width: '45%',
    height: 180,
    borderRadius: 8,
    marginBottom: 17,
    backgroundColor: '#3A3A3C',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgItem: {
    width: 100,
    height: 100,
    marginBottom: 10,
    resizeMode: 'stretch',
  },
  desc: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 15,
    color: '#fff',
  },
})
