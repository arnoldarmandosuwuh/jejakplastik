import React from 'react'
import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { BlobQuiz, ICBgMenu, ICBtnFind, ICDiamond, Pan } from '../../../assets'

const Search = ({ title, color, onPress, icon }) => {
  return (
    <>
      <TouchableOpacity
        style={styles.page}
        activeOpacity={0.7}
        onPress={onPress}
      >
        <LinearGradient
          colors={[color.color1, color.color2]}
          style={styles.container}
        >
          <Text style={styles.title}>{title}</Text>
          <View style={styles.iconImage}>
            <Image style={styles.imgStyle} source={{ uri: icon }} />
            <ICBgMenu style={styles.bgMenu} height={90} />
          </View>

          <View style={styles.btnFind}>
            <LinearGradient
              colors={[color.color1, color.color2]}
              style={styles.btn}
            >
              <Text style={styles.btnFont}>CARI</Text>
              <ICBtnFind style={styles.iconEllipse} height={72} width={72} />
            </LinearGradient>
          </View>
        </LinearGradient>
      </TouchableOpacity>
    </>
  )
}

export default Search

const styles = StyleSheet.create({
  page: {
    zIndex: 1,
    marginBottom: 47,
  },
  iconEllipse: {
    zIndex: 1,
    marginTop: 4,
  },
  btnFont: {
    zIndex: 10,
    fontSize: 13,
    color: '#fff',
    position: 'absolute',
    top: '35%',
    right: '30%',
    fontFamily: 'Montserrat-SemiBold',
  },
  btnFind: {
    position: 'absolute',
    bottom: '-15%',
    zIndex: 1,
    marginLeft: 15,
  },
  iconImage: {
    position: 'absolute',
    right: 8,
    bottom: 8,
  },
  imgStyle: { height: 100, width: 100, zIndex: 10, resizeMode: 'stretch' },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#fff',
    fontSize: 22,
    marginTop: 20,
    marginLeft: 15,
  },
  container: {
    width: '100%',
    height: 156,
    borderRadius: 8,
  },
  bgMenu: {
    width: 25,
    height: 25,
    position: 'absolute',
    bottom: 0,
  },
  btn: {
    borderRadius: 100,
  },
})
