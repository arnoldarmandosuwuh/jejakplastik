import CheckBox from '@react-native-community/checkbox';
import React from 'react';
import { StyleSheet, Text, View, Dimensions, TouchableOpacity, Image, ScrollView } from 'react-native';
import { Gap } from '../..';
import { ILTrash } from '../../../assets';

const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height
const CorouselItem = ({ item }) => {
  // const { width } = useWindowDimensions();
  return (
    <ScrollView
      pagingEnabled={true}
      showsHorizontalScrollIndicator={false}
      stickyHeaderIndices={[1]}
     
    >
      <View style={[styles.container]}>
        <Image source={ILTrash} style={styles.image} />
        <Text style={styles.title}>Apa yang terjadi pada sampah plastik jika dibiarkan di lingkungan ?</Text>

        <View style={styles.checkboxContainer}>
          <TouchableOpacity style={styles.containerChek}>
            <CheckBox
              style={styles.checkbox}
            />
            <View style={styles.conDesc}>
              <Text style={styles.desc}>Sudah melakukan</Text>
            </View>
          </TouchableOpacity>
          <Gap height={28} />
          <View style={styles.containerChek}>
            <CheckBox style={styles.checkbox} />
            <View style={styles.conDesc}>
              <Text style={styles.desc}>Belum melakukan</Text>
            </View>
          </View>
        </View>
      </View>
      <View style={styles.container}>
        <Text style={styles.titleDsc}>Apa yang terjadi pada sampah plastik jika dibiarkan di lingkungan ?</Text>
        <Text style={styles.title}>Tidak sepenuhnya hilang, hanya menjadi sangat kecil.</Text>
        <Text style={styles.normal}>Pemerintah Kabupaten Buleleng - BERAPA LAMA SAMPAH PLASTIK TERURAI? Diakses pada 30 Maret 2022)</Text>
        <TouchableOpacity activeOpacity={0.7} style={styles.conBtn}>
          <View style={styles.btn}>
            <Text style={styles.fontBtn}>Kuis Selanjutnya</Text>
          </View>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};
export default CorouselItem;

const styles = StyleSheet.create({
  conBtn: {
    marginTop: 30,
    width: '100%',
    alignItems: 'center',
    zIndex: 20
  },
  fontBtn: {
    fontSize: 15,
    fontWeight: '400',
    color: 'black',
    fontFamily: 'Montserrat-Regular',
  },
  btn: {
    backgroundColor: 'white',
    height: 41,
    width: 168,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center'
  },
  conDesc: {
    backgroundColor: '#3A3A3C',
    height: 59,
    marginLeft: 14,
    flex: 1,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center'
  },
  checkboxContainer: {
    marginHorizontal: 20,
    marginVertical: 8
  },
  containerChek: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 8
  },
  checkbox: {
    alignSelf: 'center',
    backgroundColor: 'white',
  },
  titleDsc: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    fontWeight: 'bold',
    marginTop: -140
  },
  desc: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 15,
    textAlign: 'center',
    color: 'white'
  },
  normal: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    marginTop: 10,
  },
  image: {
    width: '100%',
    height: 183
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    fontFamily: 'Montserrat-Regular',
    color: 'white',
    textAlign: 'center',
    marginHorizontal: 10,
    marginTop: 10
  },
  container: {
    width: screenWidth,
    height: screenHeight,
    backgroundColor: '#1B1818',
    zIndex: 10
  },
});
