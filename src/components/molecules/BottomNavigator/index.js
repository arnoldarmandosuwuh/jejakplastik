import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import {
  ICHome,
  ICProfile,
  ICProfileNormal,
  ICSearch,
  ICSearchNormal,
  ICSurvey,
  ICSurveyNormal,
} from '../../../assets'

const Icon = ({ label, focus }) => {
  switch (label) {
    case 'Home':
      return (
        <View style={styles.navItem}>
          <ICHome height={40} />
          <Text style={styles.label(focus)}>Beranda</Text>
        </View>
      )
    case 'Find':
      return (
        <View style={styles.navItem}>
          {focus ? <ICSearch height={40} /> : <ICSearchNormal height={40} />}
          <Text style={styles.label(focus)}>Kuis</Text>
        </View>
      )
    case 'Survey':
      return (
        <View style={styles.navItem}>
          {focus ? <ICSurvey height={40} /> : <ICSurveyNormal height={40} />}
          <Text style={styles.label(focus)}>Survey</Text>
        </View>
      )
    case 'Profile':
      return (
        <View style={styles.navItem}>
          {focus ? <ICProfile height={40} /> : <ICProfileNormal height={40} />}
          <Text style={styles.label}>Profile</Text>
        </View>
      )
    default:
      return (
        <View style={styles.navItem}>
          <ICHome height={40} />
          <Text style={styles.label}>Beranda</Text>
        </View>
      )
  }
}

const BottomNavigator = ({ state, descriptors, navigation }) => {
  const focusedOptions = descriptors[state.routes[state.index].key].options

  if (focusedOptions.tabBarVisible === false) {
    return null
  }

  return (
    <View style={styles.container}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key]

        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name

        const isFocused = state.index === index

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          })

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate(route.name)
          }
        }

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          })
        }

        return (
          <TouchableOpacity
            key={index}
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
          >
            <Icon label={label} focus={isFocused} />
          </TouchableOpacity>
        )
      })}
    </View>
  )
}

export default BottomNavigator

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingTop: 10,
    paddingBottom: 10,
    paddingHorizontal: 24,
    justifyContent: 'space-between',
    backgroundColor: '#48484AB8',
  },
  navItem: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: (focus) => ({
    fontFamily: 'Montserrat-Regular',
    fontSize: 12,
    color: focus ? '#FFFFFF' : '#1B1818',
    textAlign: 'center',
  }),
})
