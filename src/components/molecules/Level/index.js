import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
} from 'react-native'
import React from 'react'
import { BlobLanjut, BlobPemula, ICLanjut, ICPemula } from '../../../assets'

const Level = ({ onPress, text, type, active }) => {
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
      <View style={styles.container(active)}>
        {type === 'pemula' && (
          <ImageBackground
            source={BlobPemula}
            style={styles.blob}
            resizeMode="cover"
          >
            <ICPemula width={34} />
          </ImageBackground>
        )}
        {type === 'lanjut' && (
          <ImageBackground
            source={BlobLanjut}
            style={styles.blob}
            resizeMode="cover"
          >
            <ICLanjut width={123} />
          </ImageBackground>
        )}
        <Text style={styles.text}>{text}</Text>
      </View>
    </TouchableOpacity>
  )
}

export default Level

const styles = StyleSheet.create({
  container: (active) => ({
    backgroundColor: '#3A3A3C',
    borderRadius: 8,
    borderWidth: active ? 1 : 0,
    borderColor: active ? '#FFFFFF' : '',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    width: 120,
    height: 120,
    paddingHorizontal: 10,
  }),
  text: {
    color: '#FFFFFF',
    fontFamily: 'Monserrat-Regular',
    fontSize: 15,
    marginTop: 16,
    textAlign: 'center',
  },
  blob: {
    width: 50,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
