import { StyleSheet, Text, View, TextInput } from 'react-native'
import React, { useState } from 'react'

const InputForm = ({
  label,
  secureTextEntry,
  value,
  onChangeText,
  selectTextOnFocus,
  editable,
}) => {
  const [border, setBorder] = useState('#F3F3F3')
  const onFocusForm = () => {
    setBorder('#129BEB')
  }
  const onBlurForm = () => {
    setBorder('#F3F3F3')
  }
  return (
    <View>
      <TextInput
        onFocus={onFocusForm}
        onBlur={onBlurForm}
        style={styles.input(border)}
        placeholder={label}
        secureTextEntry={secureTextEntry}
        value={value}
        onChangeText={onChangeText}
        selectTextOnFocus={!selectTextOnFocus}
        editable={!editable}
      />
    </View>
  )
}

export default InputForm

const styles = StyleSheet.create({
  input: (border) => ({
    borderWidth: 1,
    borderColor: border,
    borderRadius: 15,
    padding: 20,
    fontFamily: 'Montserrat-Regular',
    fontSize: 14,
    color: '#666161',
    backgroundColor: '#FFFFFF',
  }),
})
