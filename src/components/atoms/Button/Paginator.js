import React from 'react';
import { StyleSheet, View, useWindowDimensions, Animated, Text, TouchableOpacity } from 'react-native';
import { ICDiamond } from '../../../assets';

const Paginator = ({ data, scrollx, navigation }) => {
    const { width } = useWindowDimensions();
    return (
        <View style={styles.header}>
            <View style={styles.container}>
                <ICDiamond height={42} width={46} />
                <Text style={styles.scor}>10</Text>
            </View>
            <TouchableOpacity activeOpacity={0.7} onPress={()=>navigation.replace('MainApp')} style={styles.close}>
                <Text>Tutup</Text>
            </TouchableOpacity>
            <View style={styles.conCircle}>
                <View style={styles.circle}></View>
                <View style={styles.circle}></View>
                <View style={styles.circle}></View>
            </View>
            <View style={styles.page}>
                {data.map((_, i) => {
                    const inputRange = [(i - 1) * width, 1 * width, (i + 1) * width];
                    const dotwidth = scrollx.interpolate({
                        inputRange,
                        outputRange: [16, 16, 16],
                        extrapolate: 'clamp',
                    });
                    const opacity = scrollx.interpolate({
                        inputRange,
                        outputRange: [0, 1, 1],
                        extrapolate: 'clamp',
                    });
                    return (
                        <>
                            <Animated.View
                                style={[styles.dot, { width: dotwidth, opacity }]}
                                key={i.toString()}
                            />
                        </>
                    );
                })}
            </View>
        </View>
    );
};
export default Paginator;

const styles = StyleSheet.create({
    conCircle: {
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'relative',
        zIndex: 10,
        top: 22,
    },
    circle: {
        backgroundColor: 'white',
        width: 30,
        height: 30,
        borderRadius: 99,
        marginHorizontal: 4
    },
    container: {
        alignItems: 'center',
        marginTop: 10
    },
    close: {
        fontSize: 15,
        position: 'absolute',
        top: 20,
        right: 20
    },
    scor: {
        fontSize: 20,
        fontWeight: 'bold',
        marginTop: 10
    },
    header: {
        backgroundColor: '#7CA4E9'
    },
    dot: {
        height: 16,
        width: 16,
        borderWidth: 1,
        marginHorizontal: 11,
    },
    page: {
        flexDirection: 'row',
        height: 40,
        width: '100%',
        justifyContent: 'center',
        zIndex: 20
    },
});
