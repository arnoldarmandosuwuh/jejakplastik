import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import LinearGradient from 'react-native-linear-gradient'

const Button = ({ text, onPress, type }) => {
  if (type === 'primary') {
    return (
      <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
        <LinearGradient
          colors={['#6DF6B4', '#517CCD']}
          style={styles.linearGradient}
        >
          <Text style={styles.text(type)}>{text}</Text>
        </LinearGradient>
      </TouchableOpacity>
    )
  } else if (type === 'secondary') {
    return (
      <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
        <View style={styles.container(type)}>
          <Text style={styles.text(type)}>{text}</Text>
        </View>
      </TouchableOpacity>
    )
  }
  return (
    <TouchableOpacity activeOpacity={0.7} onPress={onPress}>
      <View style={styles.container(type)}>
        <Text style={styles.text(type)}>{text}</Text>
      </View>
    </TouchableOpacity>
  )
}

export default Button

const styles = StyleSheet.create({
  container: (type) => ({
    backgroundColor: type === 'secondary' ? '#F3F3F3' : '#1B1818',
    paddingTop: 8,
    paddingBottom: 8,
    width: 340,
    borderRadius: type === 'secondary' ? 15 : 60,
    alignItems: 'center',
  }),
  linearGradient: {
    paddingTop: 8,
    paddingBottom: 8,
    width: 344,
    borderRadius: 15,
    alignItems: 'center',
  },
  text: (type) => ({
    fontFamily: 'Montserrat-SemiBold',
    fontSize: type === 'primary' || type === 'secondary' ? 18 : 20,
    color: type === 'primary' || type === 'secondary' ? '#000000' : '#FFFFFF',
    textAlign: 'center',
  }),
})
