import { showMessage } from 'react-native-flash-message'

export const showError = (message) => {
  showMessage({
    message,
    type: 'default',
    backgroundColor: '#E06379',
    color: '#FFFFFF',
  })
}

export const showSuccess = (message) => {
  showMessage({
    message,
    type: 'default',
    backgroundColor: '#6DF6B4',
    color: '#FFFFFF',
  })
}
