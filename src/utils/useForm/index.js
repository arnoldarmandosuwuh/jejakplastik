import { useState } from 'react'

export const useForm = (inititalValue) => {
  const [value, setValue] = useState(inititalValue)

  return [
    value,
    (formType, formValue) => {
      if (formType === 'reset') {
        return setValue(inititalValue)
      }
      return setValue({ ...value, [formType]: formValue })
    },
  ]
}
