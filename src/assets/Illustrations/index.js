import HomeBg from './home_bg.png'
import SplashScreenBg from './splashscreen_bg.png'
import GettingBg from './getting_started.png'
import BlobPemula from './blob_pemula.png'
import BlobLanjut from './blob_lanjut.png'
import BlobQuiz from './blob_quiz.png'
import Pan from './pan.png'
import ILLCor1 from './corousel(1).png';
import ILLCor2 from './corousel(2).png';
import ILLCor3 from './corousel(3).png';
import ILTrash from './trash.png'

export { HomeBg, SplashScreenBg, GettingBg, BlobPemula, BlobLanjut, BlobQuiz, Pan, ILTrash, ILLCor1, ILLCor2, ILLCor3 }

