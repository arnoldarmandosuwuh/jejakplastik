import ICHome from './ic_home.svg'
import ICSearch from './ic_search.svg'
import ICSearchNormal from './ic_search_normal.svg'
import ICSurvey from './ic_survey.svg'
import ICSurveyNormal from './ic_survey_normal.svg'
import ICProfile from './ic_profile.svg'
import ICProfileNormal from './ic_profile_normal.svg'
import ICBgMenu from './ic_bg_menu.svg'
import ICBtnFind from './ic_btn_find.svg'
import ICHand from './ic_hand.png'
import ICDiamond from './ic_diamonds.svg'
import ICPemula from './ic_pemula.svg'
import ICLanjut from './ic_lanjut.svg'


export {
  ICHome,
  ICSearch,
  ICSearchNormal,
  ICSurvey,
  ICSurveyNormal,
  ICProfile,
  ICProfileNormal,
  ICBgMenu,
  ICBtnFind,
  ICHand,
  ICDiamond,
  ICPemula,
  ICLanjut,

}
