import { initializeApp } from 'firebase/app'
import { getDatabase, ref, onValue} from "firebase/database";

const firebaseConfig = {
  apiKey: 'AIzaSyCQu7h1IWNTLLGdVhIM1qqVj-1Cz3oONAs',
  authDomain: 'jejak-plastik-aas.firebaseapp.com',
  databaseURL:
    'https://jejak-plastik-aas-default-rtdb.asia-southeast1.firebasedatabase.app',
  projectId: 'jejak-plastik-aas',
  storageBucket: 'jejak-plastik-aas.appspot.com',
  messagingSenderId: '60018077454',
  appId: '1:60018077454:web:6ee42c03dea1dc7dfb715f',
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)

const Fire = app

export default Fire
