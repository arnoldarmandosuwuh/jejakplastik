import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import {
  SplashScreen,
  Home,
  Welcome,
  GettingStarted,
  Register,
  Login,
  Find,
  Survey,
  Profile,
  SurveyItems,
  SurveyItemDetail,
  FindDetail,
} from '../pages'
import { BottomNavigator } from '../components'
import Paginator from '../components/atoms/Button/Paginator'

const Stack = createNativeStackNavigator()
const Tab = createBottomTabNavigator()

const MainApp = () => {
  return (
    <Tab.Navigator tabBar={(props) => <BottomNavigator {...props} />}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />
      <Tab.Screen
        name="Find"
        component={Find}
        options={{ headerShown: false }}
      />
      <Tab.Screen
        name="Survey"
        component={Survey}
        options={{ headerShown: false }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{ headerShown: false }}
      />
    </Tab.Navigator>
  )
}

const Router = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Welcome"
        component={Welcome}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="GettingStarted"
        component={GettingStarted}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="MainApp"
        component={MainApp}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SurveyItems"
        component={SurveyItems}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SurveyItemDetails"
        component={SurveyItemDetail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="FindDetail"
        component={FindDetail}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}

export default Router
