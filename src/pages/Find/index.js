import React, { useState } from 'react'
import { ScrollView, StyleSheet, Text, View } from 'react-native'
import { ICDiamond } from '../../assets'
import { Gap, Level, Menu } from '../../components'

const Find = ({navigation}) => {
  const [level, setLevel] = useState('')

  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <Gap height={24} />
          <View style={styles.headerContainer}>
            <View style={styles.titleContainer}>
              <Text style={styles.title}>
                Cari plastik di sekitar kita yuk !
              </Text>
            </View>
            <Gap width={14} />
            <View>
              <View style={styles.point}>
                <ICDiamond height={16} width={16} />
                <Gap width={5} />
                <Text style={styles.pointText}>0</Text>
              </View>
            </View>
          </View>
          <Gap height={40} />
          <View style={styles.levelContainer}>
            <Level
              text="Tingkat Pemula"
              type="pemula"
              onPress={() => setLevel('pemula')}
              active={level === 'pemula' ? true : false}
            />
            <Level
              text="Tingkat Lanjut"
              type="lanjut"
              onPress={() => setLevel('lanjut')}
              active={level === 'lanjut' ? true : false}
            />
          </View>
          <Gap height={12} />
        </View>
        <View style={styles.cardMenu}>
          <Menu menuQuiz title={'Memasak'} bgColor={'#F178B6'} onPress={()=>navigation.navigate('FindDetail')}/>
          <Menu menuQuiz title={'Bersantai'} bgColor={'#ADC965'} />
          <Menu menuQuiz title={'Membersihkan Badan'} bgColor={'#7CA4E9'} />

        </View>
      </ScrollView>
    </View>
  )
}

export default Find

const styles = StyleSheet.create({
  cardMenu: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'space-between',
    
  },
  page: {
    flex: 1,
    backgroundColor: '#000000',
    paddingHorizontal: 24,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    color: '#FFFFFF',
    fontSize: 25,
    textAlign: 'left',
  },
  point: {
    backgroundColor: 'rgba(255, 255, 255, 0.16)',
    borderRadius: 100,
    flexDirection: 'row',
    paddingHorizontal: 20,
    paddingVertical: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  pointText: {
    fontFamily: 'Montserrat-Regular',
    color: '#FFFFFF',
    fontSize: 16,
  },
  titleContainer: {
    width: '75%',
  },
  levelContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
})
