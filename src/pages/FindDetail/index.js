import CheckBox from '@react-native-community/checkbox';
import React, { useEffect, useRef, useState } from 'react'
import {
    Animated,
    FlatList,
    StyleSheet,
    TouchableOpacity,
    Text,
    View,
    Dimensions,
    ScrollView,
    Image
} from 'react-native'
import slides from './slides'
import { CorouselItem, Gap } from '../../components'
import Paginator from '../../components/atoms/Button/Paginator'
import { ILTrash } from '../../assets';

const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const GettingStarted = ({ navigation }) => {
    const [currentIndex, setCurrentIndex] = useState(0);
    const scrollx = useRef(new Animated.Value(0)).current;
    const ref = useRef(null);
    const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50 }).current;
    const [index, setIndex] = useState(0)

    const viewableItemsChange = useRef(({ viewableItems }) => {
        setCurrentIndex(viewableItems[0].index);
    }).current;

    useEffect(() => {
        ref.current?.scrollToIndex({
            index,
            Animated: true
        })
    }, [index])
    return (
        <>
            <View style={styles.container}>
                <View style={styles.page}>
                    <Paginator data={slides} scrollx={scrollx}/>
                    <FlatList
                        ref={ref}
                        initialScrollIndex={index}
                        data={slides}
                        // <CorouselItem item={item} index={findex} />
                        renderItem={({ item, index: findex }) => {
                            return (
                                <ScrollView
                                    pagingEnabled={true}
                                    showsHorizontalScrollIndicator={false}
                                    stickyHeaderIndices={[1]}
                                >
                                    <View style={[styles.containerQuiz]}>
                                        <Image source={ILTrash} style={styles.image} />
                                        <Text style={[styles.title, styles.fontFamily, styles.fontWeight, styles.fontSize(30)]}>Apa yang terjadi pada sampah plastik jika dibiarkan di lingkungan ?</Text>

                                        <View style={styles.checkboxContainer}>
                                            <TouchableOpacity style={styles.containerChek}>
                                                <CheckBox style={styles.checkbox} />
                                                <View style={styles.conDesc}>
                                                    <Text style={[styles.fontFamily, styles.fontSize(15)]}>Sudah melakukan</Text>
                                                </View>
                                            </TouchableOpacity>
                                            <Gap height={28} />
                                            <View style={styles.containerChek}>
                                                <CheckBox style={styles.checkbox} />
                                                <View style={styles.conDesc}>
                                                    <Text style={[styles.fontFamily, styles.fontSize(15)]}>Belum melakukan</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.containerQuiz}>
                                        <Text style={[styles.fontFamily, styles.fontSize(20), styles.fontWeight, styles.titleDsc]}>Apa yang terjadi pada sampah plastik jika dibiarkan di lingkungan ?</Text>
                                        <Text style={[styles.title, styles.fontFamily, styles.fontWeight, styles.fontSize(30)]}>Tidak sepenuhnya hilang, hanya menjadi sangat kecil.</Text>
                                        <Text style={[styles.normal, styles.fontFamily, styles.fontSize(20)]}>Pemerintah Kabupaten Buleleng - BERAPA LAMA SAMPAH PLASTIK TERURAI? Diakses pada 30 Maret 2022)</Text>
                                        <TouchableOpacity activeOpacity={0.7} onPress={() => {
                                            if (index === item.length - 1) {
                                                return
                                            } else if (index === 2) {
                                                return (navigation.replace('MainApp'))
                                            } else {
                                                setIndex(index + 1)
                                            }
                                        }} style={styles.conBtn}>
                                            <View style={styles.btn}>
                                                <Text style={styles.fontBtn}>Kuis Selanjutnya</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </ScrollView>
                            )
                        }}

                        horizontal
                        showsHorizontalScrollIndicator={false}
                        pagingEnabled
                        bounces={false}
                        keyExtractor={item => item.id}
                        onScroll={Animated.event(
                            [
                                {
                                    nativeEvent: { contentOffset: { x: scrollx } },
                                },
                            ],
                            {
                                useNativeDriver: false,
                            },
                        )}
                        scrollEventThrottle={32}
                        onViewableItemsChanged={viewableItemsChange}
                        viewabilityConfig={viewConfig}
                    />
                </View>

            </View>
        </>
    )
}

export default GettingStarted

const styles = StyleSheet.create({
    conBtn: {
        width: '100%',
        height: 50,
    },
    btn: {
        width: 40,
        height: 40,
        backgroundColor: 'yellow',
        borderRadius: 999,
        justifyContent: 'center',
        right: 30,
        bottom: 30,
        position: 'absolute',
        alignContent: 'center',
        alignItems: 'center',
    },
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    page: { flex: 1 },
    containerQuiz: {
        width: screenWidth,
        height: screenHeight,
        backgroundColor: '#1B1818',
        zIndex: 10
    },
    image: {
        width: '100%',
        height: 183
    },
    fontFamily: {
        fontFamily: 'Montserrat-Regular',
        color: 'white',
        textAlign: 'center',
    },
    fontWeight: {
        fontWeight: 'bold',
    },
    fontSize: (number) => ({
        fontSize: number
    }),
    title: {
        marginHorizontal: 10,
        marginTop: 10
    },
    checkboxContainer: {
        marginHorizontal: 20,
        marginVertical: 8
    },
    containerChek: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 8
    },
    checkbox: {
        alignSelf: 'center',
        backgroundColor: 'white',
    },
    conDesc: {
        backgroundColor: '#3A3A3C',
        height: 59,
        marginLeft: 14,
        flex: 1,
        borderRadius: 15,
        justifyContent: 'center',
        alignItems: 'center'
    },
    titleDsc: {
        marginTop: -140
    },
    normal: {
        marginTop: 10,
    },
    conBtn: {
        marginTop: 30,
        width: '100%',
        alignItems: 'center',
        zIndex: 20
    },
    btn: {
        backgroundColor: 'white',
        height: 41,
        width: 168,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    },
    fontBtn: {
        fontSize: 15,
        fontWeight: '400',
        color: 'black',
        fontFamily: 'Montserrat-Regular',
    },
}) 
