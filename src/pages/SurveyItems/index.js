import React, { useEffect, useState } from 'react'
import { ScrollView, StyleSheet, View, Text, Image } from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import { Gap, SurveyItem } from '../../components'
import { ICBgMenu } from '../../assets'

const SurveyItems = ({ navigation, route }) => {
  const surveyItem = route.params
  const [dataSurveyItem, setDataSurveyItem] = useState([])

  useEffect(() => {
    const data = surveyItem.surveyItem
    const filterData = data.filter((el) => el !== null)
    setDataSurveyItem(filterData)
  }, [surveyItem.surveyItem])

  return (
    <View style={styles.page}>
      <ScrollView>
        <Gap height={8} />
        <LinearGradient
          colors={[surveyItem.color.color1, surveyItem.color.color2]}
          style={styles.containerHeader}
        >
          <Text style={styles.title}>{surveyItem.name}</Text>
          <View style={styles.iconImage}>
            <Image style={styles.imgStyle} source={{ uri: surveyItem.icon }} />
            <ICBgMenu style={styles.bgMenu} height={90} />
          </View>
        </LinearGradient>
        <Gap height={24} />
        <View style={styles.containerItem}>
          {dataSurveyItem.map((item, index) => {
            return (
              <SurveyItem
                key={index}
                onPress={() =>
                  navigation.navigate('SurveyItemDetails', {
                    ...item,
                    surveyId: surveyItem.surveyId,
                    surveyItemId: item.id,
                  })
                }
                icon={item.icon}
                title={item.name}
              />
            )
          })}
        </View>
      </ScrollView>
    </View>
  )
}

export default SurveyItems

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#000000',
    paddingHorizontal: 12,
  },
  containerHeader: {
    width: '100%',
    height: 156,
    borderRadius: 8,
  },
  containerItem: {
    flexDirection: 'row',
    flex: 1,
    marginBottom: 17,
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  title: {
    fontFamily: 'Montserrat-SemiBold',
    color: '#fff',
    fontSize: 22,
    marginTop: 20,
    marginLeft: 15,
  },
  iconImage: {
    position: 'absolute',
    right: 8,
    bottom: 8,
  },
  imgStyle: { height: 100, width: 100, zIndex: 10, resizeMode: 'stretch' },
  bgMenu: {
    width: 25,
    height: 25,
    position: 'absolute',
    bottom: 0,
  },
})
