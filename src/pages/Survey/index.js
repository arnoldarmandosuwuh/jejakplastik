import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import { Menu } from '../../components/molecules'
import { Fire } from '../../config'
import { getDatabase, ref, child, get } from 'firebase/database'
import { showError } from '../../utils'

const Survey = ({ navigation }) => {
  const [dataSurvey, setDataSurvey] = useState([])

  useEffect(() => {
    const dbRef = ref(getDatabase(Fire))
    get(child(dbRef, 'survey/'))
      .then((res) => {
        if (res.val()) {
          const data = res.val()
          const filterData = data.filter((el) => el !== null)
          setDataSurvey(filterData)
        }
      })
      .catch((error) => {
        showError(error.message)
      })
  }, [])

  return (
    <ScrollView style={styles.page}>
      <Text style={styles.title}>Penggunaan plastik disekitar kita.</Text>
      <View style={styles.item}>
        {dataSurvey.map((item, index) => {
          return (
            <Menu
              key={index}
              title={item.name}
              icon={item.icon}
              color={item.color}
              onPress={() =>
                navigation.navigate('SurveyItems', {
                  ...item,
                  surveyId: item.id,
                })
              }
            />
          )
        })}
      </View>
    </ScrollView>
  )
}

export default Survey

const styles = StyleSheet.create({
  item: {
    marginTop: 18,
  },
  page: {
    flex: 1,
    backgroundColor: '#000000',
    paddingHorizontal: 12,
  },
  title: {
    color: '#FFFFFF',
    fontSize: 25,
    fontFamily: 'Montserrat-Regular',
    marginTop: 8,
  },
})
