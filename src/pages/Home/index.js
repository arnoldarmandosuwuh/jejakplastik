import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React from 'react'
import { Gap } from '../../components'

const Home = () => {
  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <Gap height={24} />
          <Text style={styles.title}>Selamat Datang</Text>
          <Gap height={16} />
          <Text style={styles.desc}>
            Aplikasi ini dibuat dalam rangka tugas sekolah dan akan selalu
            diperbarui.
          </Text>
          <Gap height={16} />
          <Text style={styles.desc}>
            Terima kasih kepada mentor dan guru atas bimbingin selama proses
            pembuatan aplikasi ini.
          </Text>
          <Gap height={24} />
        </View>
      </ScrollView>
    </View>
  )
}

export default Home

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#000000',
    paddingHorizontal: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    color: '#FFFFFF',
    fontSize: 30,
    textAlign: 'center',
  },
  desc: {
    fontFamily: 'Montserrat-Regular',
    color: '#FFFFFF',
    fontSize: 13,
    textAlign: 'center',
  },
})
