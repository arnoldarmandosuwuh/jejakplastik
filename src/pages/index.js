import SplashScreen from './SplashScreen'
import Welcome from './Welcome'
import GettingStarted from './GettingStarted'
import Register from './Register'
import Login from './Login'
import Home from './Home'
import Find from './Find'
import Survey from './Survey'
import Profile from './Profile'
import SurveyItems from './SurveyItems'
import SurveyItemDetail from './SurveyItemDetails'
import FindDetail from './FindDetail'

export {
  SplashScreen,
  Welcome,
  GettingStarted,
  Register,
  Login,
  Home,
  Find,
  Survey,
  Profile,
  SurveyItems,
  SurveyItemDetail,
  FindDetail
}
