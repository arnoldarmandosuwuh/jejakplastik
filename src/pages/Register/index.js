import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React from 'react'
import { Button, Gap, InputForm } from '../../components'
import { useForm, showError, storeData } from '../../utils'
import { getAuth, createUserWithEmailAndPassword } from 'firebase/auth'
import { getDatabase, ref, set } from 'firebase/database'
import { Fire } from '../../config'
import { useDispatch } from 'react-redux'

const Register = ({ navigation }) => {
  const [form, setForm] = useForm({
    nama: '',
    tanggalLahir: '',
    provinsi: '',
    email: '',
    password: '',
  })

  const dispatch = useDispatch()

  const onRegister = () => {
    dispatch({ type: 'SET_LOADING', value: true })
    const auth = getAuth(Fire)
    const db = getDatabase(Fire)
    createUserWithEmailAndPassword(auth, form.email, form.password)
      .then((success) => {
        const data = {
          nama: form.nama,
          tanggalLahir: form.tanggalLahir,
          provinsi: form.provinsi,
          email: form.email,
          uid: success.user.uid,
        }
        set(ref(db, 'users/' + success.user.uid), data)
        setForm('reset')
        storeData('user', data)
        dispatch({ type: 'SET_LOADING', value: false })
        navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] })
      })
      .catch((error) => {
        dispatch({ type: 'SET_LOADING', value: false })
        showError(error.message)
      })
  }

  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <Gap height={24} />
          <Text style={styles.title}>Daftar</Text>
          <Gap height={16} />
          <Text style={styles.desc}>
            Isi data biodata ini dengan jujur dan jawablah survey kami dengan
            jujur. Selama persediaan masih ada maka Anda akan mendapatkan sabun
            ramah lingkungan. Kami berjanji tidak akan menyalahgunakan data
            Anda, di luar aplikasi Jejak Plastik.
          </Text>
          <Gap height={24} />
          <InputForm
            label="Nama"
            value={form.nama}
            onChangeText={(value) => setForm('nama', value)}
          />
          <Gap height={10} />
          <InputForm
            label="Tanggal Lahir"
            value={form.tanggalLahir}
            onChangeText={(value) => setForm('tanggalLahir', value)}
          />
          <Gap height={10} />
          <InputForm
            label="Provinsi"
            value={form.provinsi}
            onChangeText={(value) => setForm('provinsi', value)}
          />
          <Gap height={10} />
          <InputForm
            label="Email"
            value={form.email}
            onChangeText={(value) => setForm('email', value)}
          />
          <Gap height={10} />
          <InputForm
            label="Password"
            secureTextEntry
            value={form.password}
            onChangeText={(value) => setForm('password', value)}
          />
          <Gap height={30} />
          <Button type="primary" text="Daftar" onPress={onRegister} />
          <Gap height={16} />
          <Button
            type="secondary"
            text="Saya sudah memiliki akun"
            onPress={() =>
              navigation.reset({ index: 0, routes: [{ name: 'Login' }] })
            }
          />
          <Gap height={24} />
        </View>
      </ScrollView>
    </View>
  )
}

export default Register

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#000000',
    flex: 1,
    paddingHorizontal: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    color: '#FFFFFF',
    fontSize: 30,
    textAlign: 'center',
  },
  desc: {
    fontFamily: 'Montserrat-Regular',
    color: '#FFFFFF',
    fontSize: 13,
    textAlign: 'center',
  },
})
