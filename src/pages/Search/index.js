import { ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { Menu } from '../../components/molecules'

const Search = () => {
 
  return (
    <ScrollView style={styles.page}>
      <Text style={styles.title}>Penggunaan plastik disekitar kita.</Text>
     
    </ScrollView>
  )
}

export default Search

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: '#000000',
    paddingHorizontal: 12
  },
  title: {
    color: '#FFFFFF',
    fontSize: 25,
    fontFamily: 'Montserrat-Regular',
    marginTop: 8
  },
})
