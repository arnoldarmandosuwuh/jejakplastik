import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React from 'react'
import { Button, Gap, InputForm } from '../../components'
import { showError, storeData, useForm } from '../../utils'
import { useDispatch } from 'react-redux'
import { getAuth, signInWithEmailAndPassword } from 'firebase/auth'
import { getDatabase, onValue, ref } from 'firebase/database'
import { Fire } from '../../config'

const Login = ({ navigation }) => {
  const [form, setForm] = useForm({
    email: '',
    password: '',
  })

  const dispatch = useDispatch()

  const onLogin = () => {
    dispatch({ type: 'SET_LOADING', value: true })
    const auth = getAuth(Fire)
    const db = getDatabase(Fire)

    signInWithEmailAndPassword(auth, form.email, form.password)
      .then((res) => {
        onValue(
          ref(db, `/users/${res.user.uid}`),
          (resDb) => {
            if (resDb.val()) {
              storeData('user', resDb.val())
              dispatch({ type: 'SET_LOADING', value: false })
              navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] })
            }
          },
          {
            onlyOnce: true,
          },
        )
      })
      .catch((err) => {
        dispatch({ type: 'SET_LOADING', value: false })
        showError(err.message)
      })
  }

  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <Gap height={24} />
          <Text style={styles.title}>Login</Text>
          <Gap height={16} />
          <Text style={styles.desc}>
            Isi data biodata ini dengan jujur dan jawablah survey kami dengan
            jujur. Selama persediaan masih ada maka Anda akan mendapatkan sabun
            ramah lingkungan. Kami berjanji tidak akan menyalahgunakan data
            Anda, di luar aplikasi Jejak Plastik.
          </Text>
          <Gap height={24} />
          <InputForm
            label="Email"
            value={form.email}
            onChangeText={(value) => setForm('email', value)}
          />
          <Gap height={10} />
          <InputForm
            label="Password"
            secureTextEntry
            value={form.password}
            onChangeText={(value) => setForm('password', value)}
          />
          <Gap height={30} />
          <Button type="primary" text="Login" onPress={onLogin} />
          <Gap height={16} />
          <Button
            type="secondary"
            text="Saya belum memiliki akun"
            onPress={() =>
              navigation.reset({ index: 0, routes: [{ name: 'Register' }] })
            }
          />
          <Gap height={24} />
        </View>
      </ScrollView>
    </View>
  )
}

export default Login

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#000000',
    flex: 1,
    paddingHorizontal: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    color: '#FFFFFF',
    fontSize: 30,
    textAlign: 'center',
  },
  desc: {
    fontFamily: 'Montserrat-Regular',
    color: '#FFFFFF',
    fontSize: 13,
    textAlign: 'center',
  },
})
