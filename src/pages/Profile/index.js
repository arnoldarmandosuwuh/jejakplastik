import { StyleSheet, Text, View, ScrollView } from 'react-native'
import React, { useEffect, useState } from 'react'
import { Button, Gap, InputForm } from '../../components'
import { Fire } from '../../config'
import {
  getAuth,
  onAuthStateChanged,
  signOut,
  updatePassword,
} from 'firebase/auth'
import { useDispatch } from 'react-redux'
import { getData, showError, showSuccess, storeData } from '../../utils'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { getDatabase, ref, set } from 'firebase/database'

const Profile = ({ navigation }) => {
  const [form, setForm] = useState({
    nama: '',
    tanggalLahir: '',
    provinsi: '',
    email: '',
  })

  const [password, setPassword] = useState('')

  useEffect(() => {
    getData('user').then((res) => {
      setForm(res)
    })
  }, [])

  const auth = getAuth(Fire)
  const db = getDatabase(Fire)

  const dispatch = useDispatch()

  const onLogout = () => {
    dispatch({ type: 'SET_LOADING', value: true })
    signOut(auth)
      .then(() => {
        AsyncStorage.removeItem('user').then(() => {
          dispatch({ type: 'SET_LOADING', value: false })
          navigation.reset({ index: 0, routes: [{ name: 'Login' }] })
        })
      })
      .catch((err) => {
        dispatch({ type: 'SET_LOADING', value: false })
        showError(err.message)
      })
  }

  const changeText = (key, value) => {
    setForm({
      ...form,
      [key]: value,
    })
  }

  const onUpdate = () => {
    dispatch({ type: 'SET_LOADING', value: true })
    if (password.length > 0) {
      if (password.length < 6) {
        dispatch({ type: 'SET_LOADING', value: false })
        showError('Password kurang dari 6 karakter')
      } else {
        onUpdatePassword()
        onUpdateProfile()
        dispatch({ type: 'SET_LOADING', value: false })
        showSuccess('Update profile dan password berhasil.')
        navigation.replace('MainApp')
      }
    } else {
      onUpdateProfile()
      dispatch({ type: 'SET_LOADING', value: false })
      showSuccess('Update profile berhasil.')
      navigation.replace('MainApp')
    }
  }

  const onUpdatePassword = () => {
    const user = auth.currentUser
    updatePassword(user, password).catch((err) => {
      dispatch({ type: 'SET_LOADING', value: false })
      showError(err.message)
    })
  }

  const onUpdateProfile = () => {
    set(ref(db, `users/${form.uid}`), form)
      .then(() => {
        storeData('user', form)
      })
      .catch((err) => {
        dispatch({ type: 'SET_LOADING', value: false })
        showError(err.message)
      })
  }

  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <Gap height={24} />
          <Text style={styles.title}>Biodata Saya</Text>
          <Gap height={16} />
          <Text style={styles.desc}>
            Isi data biodata ini dengan jujur dan jawablah survey kami dengan
            jujur. Selama persediaan masih ada maka Anda akan mendapatkan sabun
            ramah lingkungan. Kami berjanji tidak akan menyalahgunakan data
            Anda, di luar aplikasi Jejak Plastik.
          </Text>
          <Gap height={24} />
          <InputForm
            label="Nama"
            value={form.nama}
            onChangeText={(value) => changeText('nama', value)}
          />
          <Gap height={10} />
          <InputForm
            label="Tanggal Lahir"
            value={form.tanggalLahir}
            onChangeText={(value) => changeText('tanggalLahir', value)}
          />
          <Gap height={10} />
          <InputForm
            label="Provinsi"
            value={form.provinsi}
            onChangeText={(value) => changeText('provinsi', value)}
          />
          <Gap height={10} />
          <InputForm label="Email" editable value={form.email} />
          <Gap height={10} />
          <InputForm
            label="Password"
            secureTextEntry
            value={password}
            onChangeText={(value) => setPassword(value)}
          />
          <Gap height={30} />
          <Button type="primary" text="Update Profile" onPress={onUpdate} />
          <Gap height={16} />
          <Button type="secondary" text="Keluar" onPress={onLogout} />
          <Gap height={24} />
        </View>
      </ScrollView>
    </View>
  )
}

export default Profile

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#000000',
    flex: 1,
    paddingHorizontal: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    color: '#FFFFFF',
    fontSize: 30,
    textAlign: 'center',
  },
  desc: {
    fontFamily: 'Montserrat-Regular',
    color: '#FFFFFF',
    fontSize: 13,
    textAlign: 'center',
  },
})
