import { StyleSheet, Text, View, Image, ScrollView } from 'react-native'
import React from 'react'
import { GettingBg } from '../../assets'
import { Button, Gap } from '../../components'

const GettingStarted = ({ navigation }) => {
  return (
    <View style={styles.page}>
      <ScrollView>
        <View style={styles.container}>
          <Image source={GettingBg} />
          <Text style={styles.title}>Selamat Datang</Text>
          <Text style={styles.titleJejak}>di Jejak Plastik</Text>
          <Gap height={19} />
          <Text style={styles.content}>
            Kenal lebih lanjut tentang mikroplastik di sekitar kita. Isi survey
            yang ada di aplikasi ini untuk membantu kami menjadi lebih baik.
            Jika Anda memilih untuk mengisi data diri, maka Anda berkesempatan
            mendapatkan sabun ramah lingkungan (selama persediaan masih ada).
          </Text>
          <Gap height={30} />
          <Button
            type="primary"
            text="Ya, saya mau sabun ramah lingkungan"
            onPress={() =>
              navigation.reset({ index: 0, routes: [{ name: 'Register' }] })
            }
          />
          <Gap height={16} />
          <Button
            type="secondary"
            text="Saya sudah memiliki akun"
            onPress={() =>
              navigation.reset({ index: 0, routes: [{ name: 'Login' }] })
            }
          />
          <Gap height={24} />
        </View>
      </ScrollView>
    </View>
  )
}

export default GettingStarted

const styles = StyleSheet.create({
  page: {
    backgroundColor: '#000000',
    flex: 1,
  },
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
  title: {
    fontFamily: 'Montserrat-Bold',
    color: '#FFFFFF',
    fontSize: 30,
    textAlign: 'center',
    marginTop: -150,
  },
  titleJejak: {
    fontFamily: 'Montserrat-Bold',
    color: '#FFFFFF',
    fontSize: 30,
    textAlign: 'center',
  },
  content: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 14,
    textAlign: 'center',
    color: '#FFFFFF',
  },
})
