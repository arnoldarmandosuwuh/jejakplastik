import { StyleSheet, View, ImageBackground } from 'react-native'
import React, { useEffect } from 'react'
import { SplashScreenBg } from '../../assets'
import { getAuth, onAuthStateChanged } from 'firebase/auth'
import { Fire } from '../../config'

const SplashScreen = ({ navigation }) => {
  useEffect(() => {
    const auth = getAuth(Fire)
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      setTimeout(() => {
        if (user) {
          navigation.reset({ index: 0, routes: [{ name: 'MainApp' }] })
        } else {
          navigation.reset({ index: 0, routes: [{ name: 'Welcome' }] })
        }
      }, 3000)
    })

    return () => unsubscribe()
  }, [navigation])

  return (
    <View style={styles.page}>
      <ImageBackground
        source={SplashScreenBg}
        style={styles.logo}
        resizeMode="cover"
      />
    </View>
  )
}

export default SplashScreen

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  logo: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
})
