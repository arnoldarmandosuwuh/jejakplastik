import { StyleSheet, View, ImageBackground, Text } from 'react-native'
import React from 'react'
import { SplashScreenBg } from '../../assets'
import { Button, Gap } from '../../components'

const Welcome = ({ navigation }) => {
  const goToGetting = () => {
    navigation.reset({ index: 0, routes: [{ name: 'GettingStarted' }] })
  }

  return (
    <View style={styles.page}>
      <ImageBackground
        source={SplashScreenBg}
        style={styles.logo}
        resizeMode="cover"
      >
        <View style={styles.container}>
          <Text style={styles.title}>Plastic doesn’t</Text>
          <Text style={styles.title}>decompose, it just gets smaller.</Text>
          <Gap height={10} />
          <Text style={styles.subTitle}>
            It takes about 450 years just for one plastic bottle to break down
            in the ground!
          </Text>
          <Gap height={61} />
          <Button text="Mulai" type="tertiary" onPress={goToGetting} />
        </View>
      </ImageBackground>
    </View>
  )
}

export default Welcome

const styles = StyleSheet.create({
  page: {
    flex: 1,
  },
  logo: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  container: {
    backgroundColor: '#FFFFFF',
    borderTopStartRadius: 40,
    borderTopEndRadius: 40,
    paddingHorizontal: 33,
    paddingTop: 23,
    paddingBottom: 33,
    alignItems: 'center',
  },
  title: {
    fontFamily: 'Montserrat-ExtraBold',
    fontSize: 20,
    color: '#2A3447',
    textAlign: 'center',
  },
  subTitle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 16,
    color: '#707A8D',
    textAlign: 'center',
  },
})
