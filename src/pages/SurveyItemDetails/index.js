import CheckBox from '@react-native-community/checkbox'
import React, { useState } from 'react'
import {
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import { ICHand } from '../../assets'
import { Gap } from '../../components'
import { getAuth } from 'firebase/auth'
import { Fire } from '../../config'
import { getDatabase, ref, set } from 'firebase/database'
import { showError, showSuccess } from '../../utils'

const screenWidth = Dimensions.get('window').width

const SurveyItemDetail = ({ navigation, route }) => {
  const surveyItem = route.params
  const [isYes, setYes] = useState(false)
  const [isNo, setNo] = useState(false)

  const auth = getAuth(Fire)
  const db = getDatabase(Fire)
  const user = auth.currentUser

  const yes = () => {
    setYes(true)
    setNo(false)

    const data = {
      surveyId: surveyItem.surveyId,
      surveyItemId: surveyItem.surveyItemId,
      answer: 'Sudah Melakukan',
      uid: user.uid,
    }

    set(
      ref(
        db,
        `resultSurvey/${user.uid}${surveyItem.surveyId}${surveyItem.surveyItemId}`,
      ),
      data,
    )
      .then(() => {
        showSuccess('Anda telah mengisi survey')
        navigation.replace('MainApp')
      })
      .catch((error) => {
        showError(error.message)
      })
  }
  const no = () => {
    setYes(false)
    setNo(true)

    const data = {
      surveyId: surveyItem.surveyId,
      surveyItemId: surveyItem.surveyItemId,
      answer: 'Belum Melakukan',
      uid: user.uid,
    }

    set(
      ref(
        db,
        `resultSurvey/${user.uid}${surveyItem.surveyId}${surveyItem.surveyItemId}`,
      ),
      data,
    )
      .then(() => {
        showSuccess('Anda telah mengisi survey')
        navigation.replace('MainApp')
      })
      .catch((error) => {
        showError(error.message)
      })
  }

  return (
    <ScrollView
      horizontal={true}
      pagingEnabled={true}
      showsHorizontalScrollIndicator={false}
    >
      <View style={[styles.container, styles.bgColorBlack]}>
        <TouchableOpacity
          style={styles.btnClose}
          activeOpacity={0.7}
          onPress={() => navigation.goBack()}
        >
          <Text style={[styles.fontColorWhite]}>Tutup</Text>
        </TouchableOpacity>

        <View style={{ alignItems: 'center' }}>
          <Image source={{ uri: surveyItem.icon }} style={styles.image} />
          <Text style={[styles.fontColorWhite, styles.title]}>
            {surveyItem.name}
          </Text>
          <Text style={[styles.fontColorWhite, styles.desc]}>
            {surveyItem.desc}
          </Text>
        </View>
        <Image source={ICHand} style={styles.imgHand} />
      </View>

      <View style={[styles.container, styles.bgColorLight]}>
        <TouchableOpacity
          style={styles.btnClose}
          activeOpacity={0.7}
          onPress={() => navigation.goBack()}
        >
          <Text>Tutup</Text>
        </TouchableOpacity>
        <Image source={{ uri: surveyItem.survey.icon }} style={styles.image} />
        <Text style={styles.title}>{surveyItem.survey.name}</Text>
        <Text style={styles.desc}>{surveyItem.survey.question}</Text>

        <View style={styles.checkboxContainer}>
          <TouchableOpacity onPress={yes} style={styles.containerChek}>
            <CheckBox
              value={isYes}
              onValueChange={yes}
              style={styles.checkbox}
            />
            <Text style={styles.desc}>Sudah melakukan</Text>
          </TouchableOpacity>
          <Gap height={46} />
          <View style={styles.containerChek}>
            <CheckBox value={isNo} onValueChange={no} style={styles.checkbox} />
            <Text style={styles.desc}>Belum melakukan</Text>
          </View>
        </View>
      </View>
    </ScrollView>
  )
}

export default SurveyItemDetail

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    width: screenWidth,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 8,
  },
  bgColorBlack: {
    backgroundColor: '#1B1818',
  },
  bgColorLight: {
    backgroundColor: '#C4C4C4',
  },
  image: {
    width: 200,
    height: 200,
  },
  fontColorWhite: {
    color: '#fff',
  },
  title: {
    fontSize: 24,
    fontFamily: 'Montserrat-ExtraBold',
    marginTop: 50,
    marginBottom: 20,
    textAlign: 'center',
  },
  desc: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 15,
    textAlign: 'center',
  },
  imgHand: {
    position: 'absolute',
    bottom: 10,
  },
  btnClose: {
    position: 'absolute',
    right: 18,
    top: 18,
    fontSize: 15,
  },
  checkbox: {
    alignSelf: 'center',
  },
  containerChek: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkboxContainer: {
    position: 'absolute',
    bottom: 20,
  },
})
